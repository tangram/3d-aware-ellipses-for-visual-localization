# This file is part of 3D-Aware-Ellipses-for-Visual-Localization.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# 3D-Aware-Ellipses-for-Visual-Localization is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# 3D-Aware-Ellipses-for-Visual-Localization is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with 3D-Aware-Ellipses-for-Visual-Localization.
# If not, see <http://www.gnu.org/licenses/>.

class Config():
    def __init__(self):
        pass
    
    
_cfg = Config()


# Data
_cfg.CROP_SIZE = 256

# Data Transformations (use None to disable)
# ColorJitter: brightness, contrast, saturation, hue
# The values are chosen uniformly in the following intervals
# [1-BRIGHTNESS, 1+BRIGHTNESS]
# [1-CONTRAST, 1+CONTRAST]
# [1-SATURATION, 1+SATURATION]
# [-HUE, HUE]
_cfg.TRAIN_COLOR_JITTER = [0.1, 0.1, 0.1, 0.05]
_cfg.TRAIN_RANDOM_SHIFT = [-20, 20]
_cfg.TRAIN_RANDOM_ROTATION = [-30, 30]
_cfg.TRAIN_RANDOM_PERSPECTIVE = [-5, 5]
_cfg.TRAIN_RANDOM_BLUR = 3
_cfg.TRAIN_PROBA_DO_AUGM = 0.5   # probability of applyin each of the augmentations

_cfg.VALID_COLOR_JITTER = [0.0, 0.0, 0.0, 0.0]
_cfg.VALID_RANDOM_SHIFT = None
_cfg.VALID_RANDOM_ROTATION = None
_cfg.VALID_RANDOM_PERSPECTIVE = None
_cfg.VALID_RANDOM_BLUR = None
_cfg.VALID_PROBA_AUGM = 0.0  # probability of applyin each of the augmentations


# Training
_cfg.TRAIN_BATCH_SIZE = 16
_cfg.TRAIN_BATCH_SHUFFLE = True
_cfg.TRAIN_LOADER_NUM_WORKERS = 16

_cfg.VALID_BATCH_SIZE = 16
_cfg.VALID_BATCH_SHUFFLE = False
_cfg.VALID_LOADER_NUM_WORKERS = 16

_cfg.LEARNING_RATE = 5e-5


# Loss
_cfg.LOSS_SAMPLING_X_INTERVAL = [-2, 12]
_cfg.LOSS_SAMPLING_Y_INTERVAL = [-2, 12]
_cfg.LOSS_SAMPLING_X_NUM = 25
_cfg.LOSS_SAMPLING_Y_NUM = 25
_cfg.LOSS_SAMPLING_SCALE = 10


# Detector
_cfg.DETECTOR_ARCHITECTURE = "COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml"
_cfg.DETECTOR_NUM_CLASSES = 7
_cfg.DETECTOR_SCORE_THRESH_TEST = 0.5
_cfg.DETECTOR_DATALOADER_NUM_WORKERS = 8
_cfg.DETECTOR_IMS_PER_BATCH = 2
_cfg.DETECTOR_BASE_LR = 0.00025
_cfg.DETECTOR_NB_EPOCHS = 2000
_cfg.DETECTOR_BATCH_SIZE_PER_IMAGE = 64
