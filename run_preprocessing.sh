# This file is part of 3D-Aware-Ellipses-for-Visual-Localization.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# 3D-Aware-Ellipses-for-Visual-Localization is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# 3D-Aware-Ellipses-for-Visual-Localization is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with 3D-Aware-Ellipses-for-Visual-Localization.
# If not, see <http://www.gnu.org/licenses/>.

python prepare_7-Scenes.py -i $1/seq-01 $1/seq-04 $1/seq-06 -o 7-Scenes_Chess_dataset_train.json
python prepare_7-Scenes.py -i $1/seq-02 $1/seq-03 $1/seq-05 -o 7-Scenes_Chess_dataset_test.json

python annotate_objects.py data/7-Scenes_Chess_scene.json 7-Scenes_Chess_dataset_train.json 7-Scenes_Chess_dataset_train_with_obj_annot.json
python annotate_objects.py data/7-Scenes_Chess_scene.json 7-Scenes_Chess_dataset_test.json  7-Scenes_Chess_dataset_test_with_obj_annot.json
