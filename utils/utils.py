# This file is part of 3D-Aware-Ellipses-for-Visual-Localization.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# 3D-Aware-Ellipses-for-Visual-Localization is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# 3D-Aware-Ellipses-for-Visual-Localization is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with 3D-Aware-Ellipses-for-Visual-Localization.
# If not, see <http://www.gnu.org/licenses/>.

import math
import os


def force_square_bbox(bbox, margin=0):
    """
        Transform a box into a square box.
        - bbox : 2x2 matrix with first corner on first row and second corner 
                 on the second row or 1D list [x_min, y_min, x_max, y_max]
        - margin: (optional) margin applied on the left, right top and bottom
    """
    x1, y1, x2, y2 = bbox.flatten().tolist()
    w = x2 - x1 + 1
    h = y2 - y1 + 1
    d = math.ceil(max(w, h) / 2)
    d += margin

    cx = round((x1 + x2) / 2)
    cy = round((y1 + y2) / 2)

    return list(map(int, [cx-d, cy-d, cx+d, cy+d]))


def create_if_needed(folder):
    if not os.path.isdir(folder):
        os.makedirs(folder)

