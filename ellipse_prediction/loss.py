# This file is part of 3D-Aware-Ellipses-for-Visual-Localization.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# 3D-Aware-Ellipses-for-Visual-Localization is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# 3D-Aware-Ellipses-for-Visual-Localization is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with 3D-Aware-Ellipses-for-Visual-Localization.
# If not, see <http://www.gnu.org/licenses/>.

import pytorch_lightning as pl
import torch


class SamplingBasedLoss(pl.LightningModule):
    def __init__(self, range_x, range_y, sampling_x, sampling_y, scale):
        super(SamplingBasedLoss, self).__init__()
        self.scale = scale
        self.generate_sampling_points(range_x, range_y, sampling_x, sampling_y)
        
    
    def generate_sampling_points(self, range_x, range_y, sampling_x, sampling_y):
        samples_x = torch.linspace(*range_x, sampling_x)
        samples_y = torch.linspace(*range_y, sampling_y)
        # samples_x = torch.linspace(xmin, xmax, sampling_x, device=self.device)
        # samples_y = torch.linspace(ymin, ymax, sampling_y, device=self.device)
        y, x = torch.meshgrid(samples_y, samples_x)
        self.sampling_pts = torch.cat((x.flatten().view((1, -1)), 
                                       y.flatten().view((1, -1))), 0)
        # self.sampling_pts = nn.Parameter(torch.cat((x.flatten().view((1, -1)), 
        #                                             y.flatten().view((1, -1))), 0))

    def sample_ellipse(self, axes, sin, center):
        A = torch.diag(axes)
        cos = torch.sqrt(1 - sin**2)
        R = torch.cat((torch.cat((cos, -sin), dim=1),
                       torch.cat((sin, cos), dim=1)), dim=0)
        
        pts_centered = self.sampling_pts.T - center
        M = R @ A @ R.T @ pts_centered.T
        return torch.einsum("ij,ji->i", pts_centered, M)
        
    def forward(self, X, Y):
        X[:, :4] *= self.scale
        Y[:, :4] *= self.scale
        vals = []
        for i in range(X.shape[0]):
            pts_X = self.sample_ellipse(X[i, :2], X[i, 4].view((1, 1)), X[i, 2:4])
            pts_Y = self.sample_ellipse(Y[i, :2], Y[i, 4].view((1, 1)), Y[i, 2:4])
            vals.append((torch.sqrt(torch.sum((pts_X - pts_Y)**2)) / pts_X.shape[0]).unsqueeze(0))
        return torch.sum(torch.cat(vals))